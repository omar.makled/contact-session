<?php

Route::get('/',[
	'as'	=>	'messages.index',
	'uses'	=>	'MessagesController@index'
]);

Route::get('create',[
	'as'	=>	'messages.create',
	'uses'	=>	'MessagesController@create'
]);

Route::post('/',[
	'as'	=>	'messages.store',
	'uses'	=>	'MessagesController@store'
]);

Route::get('{id}',[
	'as'	=>	'messages.show',
	'uses'	=>	'MessagesController@show'
]);
