var Messages = function() {
    function convert(from, to, value) {
        var calendar, date;
        calendar = $.calendars.instance(from);
        date = calendar.parseDate('yyyy-mm-dd', value);

        calendar = $.calendars.instance(to);
        date = calendar.fromJD(date.toJD());
        return calendar.formatDate('yyyy-mm-dd', date);
    }
    var handelHijri = function() {
        // init hijri calendar
        var calendar = $.calendars.instance('UmmAlQura', 'ar');
        var escort = $('.g-date');
        $('.h-date').calendarsPicker({
            calendar: calendar,
            dateFormat: "yyyy-mm-dd",
            onSelect: function(date) {
                $(escort).val(convert('ummalqura', 'gregorian', $(this).val()));
            }
        });
    }

    var handelDataTable = function() {
        $('.data-table').DataTable({
            "order": [
                [0, "desc"]
            ]
        });
    }

    var handelGregorian = function() {
        // init gregorian calendar
        var calendar = $.calendars.instance();
        var escort = $('.h-date');
        $('.g-date').calendarsPicker({
            calendar: calendar,
            dateFormat: "yyyy-mm-dd",
            onSelect: function(date) {
                $(escort).val(convert('gregorian', 'ummalqura', $(this).val()));
            }
        });
    }
    return {
        init: function() {
            handelHijri();
            handelGregorian();
            handelDataTable();
        }
    }
}();
jQuery(document).ready(function() {
    Messages.init();
});