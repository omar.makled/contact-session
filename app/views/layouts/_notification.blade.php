<div class="row">
	<div class="col-md-12">
		@if($errors->has())
		      <div class="alert alert-danger">
		          <ul>
		          @foreach($errors->all() as $error)
		            <li>{{ $error }}</li>
		          @endforeach
		          </ul>
		      </div>
		  @endif
		  @if(Session::has('success'))
		      <div class="alert alert-success">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            {{Session::get('success')}}
		      </div>
		  @endif
		  @if(Session::has('error'))
		      <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            {{Session::get('error')}}
		      </div>
		  @endif
  		  @if(Session::has('info') and ! empty(Session::get('info')))
		      <div class="alert alert-info">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            {{Session::get('info')}}
		      </div>
		  @endif
	</div>
</div>