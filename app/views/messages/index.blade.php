@extends('layouts.main')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		Messages
	</div>
	<div class="panel-body">
		<table class="table table-striped table-bordered dt-responsive nowrap data-table" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>id</th>
					<th>Title</th>
					<th>Sender Name</th>
					<th>Sender Email</th>
					<th>Date</th>
					<th>Body</th>
				</tr>
			</thead>
			<tbody>
				@foreach($messages as $message)
				<tr>
					<td>{{ $message['timestamp'] }}</td>
					<td>{{ $message['title'] }}</td>
					<td>{{ $message['sender_name'] }}</td>
					<td>{{ $message['sender_email'] }}</td>
					<td>{{ $message['formated_date'] }} </td>
					<td>{{ $message['body'] }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>	

@stop