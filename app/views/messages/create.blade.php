@extends('layouts.main')
@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="panel-title">Send New Message
		</div>
	</div>
	<div class="panel-body">
		{{ Form::open(['route' => ['messages.store'],'method' => 'post', 'autocomplete' => 'off']) }}		
		
		<div class="form-group {{ ($errors->has('title')) ? 'has-error' : '' }}">
			{{ Form::label('title','Title') }}
			{{ Form::text('title',null,['class' => 'form-control']) }}
			{{ $errors->first('title', '<span class="help-inline text-danger">:message</span>') }}
		</div>
		
		<div class="form-group {{ ($errors->has('sender_name')) ? 'has-error' : '' }}">
			{{ Form::label('sender_name','Sender Name') }}
			{{ Form::text('sender_name',null,['class' => 'form-control']) }}
			{{ $errors->first('sender_name', '<span class="help-inline text-danger">:message</span>') }}
		</div>

		<div class="form-group {{ ($errors->has('sender_email')) ? 'has-error' : '' }}">
			{{ Form::label('sender_email','Sender Email') }}
			{{ Form::text('sender_email',null,['class' => 'form-control']) }}
			{{ $errors->first('sender_email', '<span class="help-inline text-danger">:message</span>') }}
		</div>
	
		<div class="form-group  {{ ($errors->has('body')) ? 'has-error' : '' }}">
			{{ Form::label('body','Body') }}
			{{ Form::textarea('body',null,['class' => 'form-control']) }}
			{{ $errors->first('body', '<span class="help-inline text-danger">:message</span>') }}
		</div>
		
		<div class="form-group {{ ($errors->has('hijri_date')) ? 'has-error' : '' }}">
			{{ Form::label('hijri_date','Hijri Date') }}
			{{ Form::text('hijri_date',null,['class' => 'form-control h-date']) }}
			{{ $errors->first('hijri_date', '<span class="help-inline text-danger">:message</span>') }}
		</div>

		<div class="form-group {{ ($errors->has('gregorian_date')) ? 'has-error' : '' }}">
			{{ Form::label('gregorian_date','Gregorian Date') }}
			{{ Form::text('gregorian_date',null,['class' => 'form-control g-date']) }}
			{{ $errors->first('gregorian_date', '<span class="help-inline text-danger">:message</span>') }}
		</div>
		
		<button class="btn btn-lg btn-primary btn-block" type="submit">Send</button>
		{{ Form::close() }}
	</div>
</div>
@stop