<?php

use Carbon\Carbon;

class MessagesController extends BaseController {

	protected $rules = [
        'title'             =>  'required|min:4',
        'sender_name'       =>  'required',
        'sender_email'      =>  'required|email',
        'body'              =>  'required|min:4',
        'hijri_date'		=>	'required',
        'gregorian_date'	=>	'required'
    ];

    protected $hijriMonths = [
		'01'	=> 'محرّم', 
		'02'	=> 'صفر', 
		'03'	=> 'ربيع الأول', 
		'04'	=>	'ربيع الثاني', 
		'05'	=> 'جمادى الاول', 
		'06'	=>	'جمادى الآخر', 
		'07'	=>	'رجب', 
		'08'	=>	'شعبان', 
		'09'	=>	'رمضان', 
		'10'	=>	'شوّال', 
		'11'	=>	'ذو القعدة', 
		'12'	=>	'ذو الحجة'
    ];

    protected $translateMonths = [
    	'Jan'	=>	'يناير',
    	'Feb'	=>	'فبراير',
    	'Mar'	=>	'مارس',
    	'Apr'	=>	'أبريل',
    	'May'	=>	'مايو',
    	'Jun'	=>	'يونية',
    	'Jul'	=>	'يوليو',
    	'Aug'	=>	'أغسطس',
    	'Sep'	=>	'سبتمبر',
    	'Oct'	=>	'أكتوبر',
    	'Nov'	=>	'نوفمبر',
    	'Dec'	=>	'ديسمبر',
    ];

    protected $translateDays = [
    	'Monday'	=>	'الإثنين',
    	'Tuesday'	=>	'الثلاثاء',
    	'Wednesday'	=>	'الأربعاء',
    	'Thursday'	=>	'الخميس',
    	'Friday'	=>	'الجمعة',
    	'Saturday'	=>	'السبت',
    	'Sunday'	=>	'الأحد',
    ];

	public function index()
	{
		$messages = $this->praseDate();

		return View::make('messages.index',compact('messages'));
	}

	public function create()
	{
		return View::make('messages.create');
	}

	public function store()
	{

		$validator = Validator::make(Input::all(),$this->rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
		}

		$messages = Session::get('messages');
		$messages[] = Input::except('_token');

		Session::put('messages',$messages);

		return Redirect::to('/')->withSuccess('Message has been saved !');
	}

	public function show($id)
	{

	}

	private function praseDate()
	{
		$messages = (Session::get('messages')) ?: [];
		foreach ($messages as $key => $message) {
			$messages[$key]['formated_date'] = sprintf('<span>%s</span> <br/> <span>%s</span>',
				$this->parseGregorianDate($message['gregorian_date']),
				$this->parseHijriDate($message['hijri_date'])
			);
			$messages[$key]['timestamp'] = strtotime($message['gregorian_date']);
		}

		return $messages;
	}

	private function parseGregorianDate($date)
	{
		$date = explode('-',Carbon::createFromFormat('Y-m-d', $date)->format("l-d-M-Y"));
		$date[0] = $this->translateDays[$date[0]];
		$date[2] = $this->translateMonths[$date[2]];

		return implode(' ', $date);
	}

	private function parseHijriDate($date)
	{
		$date = explode('-', $date);

		$date[1] = $this->hijriMonths[$date[1]];

		return implode(' ', $date);
	}

}