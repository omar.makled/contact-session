var gulp = require('gulp');
var concat = require('gulp-concat'); 
var minifycss = require('gulp-minify-css');
var uglify = require('gulp-uglify');

var src = 'app/assets/';
var dest = 'public/assets/';

gulp.task('css', function () { 
    return gulp.src([
            src + 'css/bootstrap.css',
            src + 'css/datepicker.css',
            src + 'css/jquery.calendars.picker.css',
            src + 'css/dataTables.bootstrap.min.css',
            src + 'css/responsive.bootstrap.min.css',
            src + 'css/custom.css',
        ])
        .pipe(minifycss())
        .pipe(concat('main.css'))
        .pipe(gulp.dest(dest + 'css'));
});

gulp.task('js', function () { 
    return gulp.src([ 
            src + 'js/jquery-1.10.2.min.js',
            src + 'js/jquery.plugin.js',
            src + 'js/bootstrap.js',
            src + 'js/jquery.datepick.js',
            src + 'js/jquery.calendars.js',
            src + 'js/jquery.calendars.plus.js',
            src + 'js/jquery.calendars.picker.js',
            src + 'js/jquery.calendars.UmmAlQura.js',
            src + 'js/jquery.calendars.ummalqura-ar.js',
            src + 'js/jquery.dataTables.min.js',
            src + 'js/dataTables.bootstrap.min.js',
            src + 'js/dataTables.bootstrap.min.js',
            src + 'js/dataTables.responsive.min.js',
            src + 'js/custom.js'
        ])
        .pipe(uglify())
        .pipe(concat('main.js'))
        .pipe(gulp.dest(dest + 'js'));
});

gulp.task('fonts', function() {
    return gulp.src([src + 'fonts/**/*'])
        .pipe(gulp.dest(dest + 'fonts'));
})

gulp.task('img', function() {
    return gulp.src([src + 'img/**/*'])
        .pipe(gulp.dest(dest + 'img'));
})

gulp.task('default',['css','js','fonts','img']);